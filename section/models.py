from django.db import models
from baseapp.models import BaseModel


class Level(BaseModel):
    name = models.CharField(max_length=255)
    index = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "section_level"

    def __str__(self) -> str:
        return self.name


class Week(BaseModel):
    level = models.ForeignKey(
        Level, on_delete=models.CASCADE, related_name='weeks')
    name = models.CharField(max_length=255)
    index = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "section_week"

    def __str__(self) -> str:
        return self.name


class Day(BaseModel):
    week = models.ForeignKey(
        Week, on_delete=models.CASCADE, related_name='days')
    name = models.CharField(max_length=255)
    index = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "section_day"

    def __str__(self) -> str:
        return self.name

    @property
    def level(self):
        return self.week.level.name

    @property
    def full_level(self):
        '%s - %s - %s' % (self.week.level, self.week, self.name)


class GrammarPoint(BaseModel):
    day = models.ForeignKey(
        Day, on_delete=models.CASCADE, related_name='grammars')
    name = models.CharField(max_length=255)
    description = models.TextField()
    index = models.IntegerField(null=True, blank=True)
    prev = models.ForeignKey('self', on_delete=models.SET_NULL,
                             related_name='next', default=None, null=True, blank=True)

    class Meta:
        db_table = "grammar_point"

    def __str__(self) -> str:
        return self.name

    @property
    def level(self):
        return self.day.week.level.name

    @property
    def week(self):
        return self.day.week.name


class Kanji(BaseModel):
    days = models.ManyToManyField(
        Day, related_name='kanji', through='KanjiDay')
    kanji = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    icon = models.FileField(upload_to='kanji', null=True, blank=True)
    writing = models.FileField(upload_to='kanji', null=True, blank=True)

    class Meta:
        db_table = "kanji"

    def __str__(self) -> str:
        return self.kanji


class KanjiDay(models.Model):
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    kanji = models.ForeignKey(Kanji, on_delete=models.CASCADE)
    index = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "kanji_day"

    def __str__(self):
        return '%s - %s - %s' % (self.day.week.level, self.day.week, self.day)


class Word(BaseModel):
    days = models.ManyToManyField(
        Day, related_name='words', through='WordsDay')
    kanji_list = models.ManyToManyField(
        Kanji, related_name='words', through='WordsKanji')
    word = models.CharField(max_length=100)
    kana = models.CharField(max_length=100, null=True, blank=True)
    translation = models.TextField(null=True, blank=True)

    class Meta:
        db_table = "word"

    def __str__(self) -> str:
        return '%s (%s)' % (self.word, self.kana)


class WordsDay(models.Model):
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    index = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "words_day"


class WordsKanji(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    kanji = models.ForeignKey(Kanji, on_delete=models.CASCADE)
    index = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = "words_kanji"
