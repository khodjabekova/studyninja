from .models import Kanji, KanjiDay, Level, Week, Day, GrammarPoint
from rest_framework import serializers


class GrammarPointListSerializer(serializers.ModelSerializer):

    class Meta:
        model = GrammarPoint
        fields = ('index', 'name',)


class DayListSerializer(serializers.ModelSerializer):
    sub_section = GrammarPointListSerializer(many=True, source='grammars')

    class Meta:
        model = Day
        fields = ('index', 'name', 'sub_section')


class WeekListSerializer(serializers.ModelSerializer):
    sub_section = DayListSerializer(many=True, source='days')

    class Meta:
        model = Week
        fields = ('index', 'name', 'sub_section')


class LevelListSerializer(serializers.ModelSerializer):
    sub_section = WeekListSerializer(many=True, source='weeks')

    class Meta:
        model = Level
        fields = ('index', 'name', 'sub_section')

#########################################################################################


class KanjiSerializer(serializers.ModelSerializer):
    kanji = serializers.CharField(source='kanji.kanji')
    icon = serializers.FileField(source='kanji.icon')

    class Meta:
        model = KanjiDay
        fields = ('kanji', 'icon', 'index')


class KanjiDetailSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='kanji')
    index = serializers.IntegerField(source='id')

    class Meta:
        model = Kanji
        fields = ('name', 'description', 'icon', 'writing', 'index')


class KanjiDetailListSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='kanji')
    index = serializers.IntegerField(source='id')

    class Meta:
        model = Kanji
        fields = ('index', 'name', )


class DayKanjiListSerializer(serializers.ModelSerializer):
    sub_section = KanjiDetailListSerializer(many=True, source='kanji')

    class Meta:
        model = Day
        fields = ('index', 'name', 'sub_section')


class WeekKanjiListSerializer(serializers.ModelSerializer):
    sub_section = DayKanjiListSerializer(many=True, source='days')

    class Meta:
        model = Week
        fields = ('index', 'name', 'sub_section')


class KanjiListSerializer(serializers.ModelSerializer):
    sub_section = WeekKanjiListSerializer(many=True, source='weeks')

    class Meta:
        model = Level
        fields = ('index', 'name', 'sub_section')


#########################################################################################

class LevelDetailsSerializer(serializers.ModelSerializer):
    weeks = WeekListSerializer(many=True)

    class Meta:
        model = Level
        fields = ('index', 'name', 'weeks')


class WeekDetailsSerializer(serializers.ModelSerializer):
    days = DayListSerializer(many=True)

    class Meta:
        model = Week
        fields = ('index', 'name', 'days')


class DayDetailsSerializer(serializers.ModelSerializer):
    grammars = GrammarPointListSerializer(many=True)

    class Meta:
        model = Day
        fields = ('index', 'name', 'grammars')


class GrammarPointDetailsSerializer(serializers.ModelSerializer):
    prev = serializers.SerializerMethodField()
    next = serializers.SerializerMethodField()

    class Meta:
        model = GrammarPoint
        fields = ('index', 'name', 'description', 'prev', 'next', 'index')

    def get_prev(self, obj):
        if obj.prev:
            return f'{obj.prev.day.week.level.index}/{obj.prev.day.week.index}/{obj.prev.day.index}/{obj.prev.index}'
        else:
            return None

    def get_next(self, obj):
        if obj.next:
            next_grammar = obj.next.get()
            return f'{next_grammar.day.week.level.index}/{next_grammar.day.week.index}/{next_grammar.day.index}/{next_grammar.index}'
        else:
            return None
