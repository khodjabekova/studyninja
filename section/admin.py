from django.forms.models import BaseInlineFormSet
from django import forms
from django.contrib import admin
from tinymce.widgets import TinyMCE
from .models import Level, Week, Day, GrammarPoint, Kanji, Word


class DayChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return '%s - %s - %s' % (obj.week.level, obj.week, obj.name)


class PrevChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return '%s - %s - %s - %s' % (obj.day.week.level, obj.day.week, obj.day, obj.name)


@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    exclude = (
        'created_by',
        'updated_by',
    )

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.updated_by = request.user
        return super().save_model(request, obj, form, change)


class DayInlines(admin.TabularInline):
    model = Day
    extra = 6
    exclude = (
        'created_by',
        'updated_by',
    )


class KanjiDayInlineForm(forms.ModelForm):

    class Meta:
        model = Day.kanji.through
        fields = ('day', 'index')

    def __init__(self, *args, **kwargs):
        super(KanjiDayInlineForm, self).__init__(*args, **kwargs)
        self.fields['day'] = DayChoiceField(queryset=Day.objects.all(), required=False)


class KanjiDayInlines(admin.TabularInline):
    model = Day.kanji.through
    form = KanjiDayInlineForm


class KanjiInlines(admin.TabularInline):
    model = Kanji
    exclude = (
        'created_by',
        'updated_by',
    )


class WordInlines(admin.TabularInline):
    model = Word.kanji_list.through
    exclude = (
        'created_by',
        'updated_by',
    )


@admin.register(Week)
class WeekAdmin(admin.ModelAdmin):
    list_display = ['name', 'level']
    search_fields = ['name', ]
    # list_filter = ['level', ]
    exclude = (
        'created_by',
        'updated_by',
    )
    inlines = [DayInlines]

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.updated_by = request.user
        return super().save_model(request, obj, form, change)


class GrammarPointForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE())

    def __init__(self, *args, **kwargs):
        super(GrammarPointForm, self).__init__(*args, **kwargs)
        self.fields['day'] = DayChoiceField(queryset=Day.objects.all(), required=False)
        self.fields['prev'] = PrevChoiceField(
            queryset=GrammarPoint.objects.all(), required=False)

    class Meta:
        model = GrammarPoint
        exclude = (
            'created_by',
            'updated_by',
        )


@admin.register(GrammarPoint)
class GrammarPointAdmin(admin.ModelAdmin):
    list_display = ['name', 'index', 'day', 'week', 'level']
    search_fields = ['name', ]
    # list_filter = ['day', 'week', 'level']

    form = GrammarPointForm

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.updated_by = request.user
        return super().save_model(request, obj, form, change)


@admin.register(Kanji)
class KanjiAdmin(admin.ModelAdmin):
    list_display = ['kanji', 'level']
    exclude = (
        'created_by',
        'updated_by',
    )

    inlines = [WordInlines, KanjiDayInlines]
    def level(self, obj):
        return "\n".join([p.level for p in obj.days.all()])
    
    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.updated_by = request.user
        return super().save_model(request, obj, form, change)


class WordsDayInlineForm(forms.ModelForm):

    class Meta:
        model = Day.words.through
        fields = ('day', 'index')

    def __init__(self, *args, **kwargs):
        super(WordsDayInlineForm, self).__init__(*args, **kwargs)
        self.fields['day'] = DayChoiceField(queryset=Day.objects.all(), required=False)


class WordsDayInlines(admin.TabularInline):
    model = Day.words.through
    form = WordsDayInlineForm


class WordsKanjiInlines(admin.TabularInline):
    model = Kanji.words.through
    exclude = (
        'created_by',
        'updated_by',
    )

@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    list_display = ['word', ]
    exclude = (
        'created_by',
        'updated_by',
    )
    inlines = [WordsDayInlines, WordsKanjiInlines]

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.updated_by = request.user
        return super().save_model(request, obj, form, change)
