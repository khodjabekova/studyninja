from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework import status
from rest_framework.views import APIView


from .models import KanjiDay, Level, Week, Day, GrammarPoint, Kanji
from . import serializers
from config.paginations import CustomPagination

# Create your views here.


class LevelListAPIView(ListAPIView):
    queryset = Level.objects.all()
    serializer_class = serializers.LevelListSerializer
    # pagination_class = CustomPagination


class KanjiListAPIView(ListAPIView):
    queryset = Level.objects.all()
    serializer_class = serializers.KanjiListSerializer

from django.db.models import Max
class KanjiIconListAPIView(ListAPIView):
    serializer_class = serializers.KanjiSerializer

    def get_queryset(self):
        level = self.kwargs['level']
        queryset = KanjiDay.objects.filter(day__week__level__index=level).order_by('index')
        return queryset

class KanjiDetailsAPIView(RetrieveAPIView):
    queryset = Kanji.objects.all()
    serializer_class = serializers.KanjiDetailSerializer



class LevelDetailsAPIView(RetrieveAPIView):
    queryset = Level.objects.all()
    serializer_class = serializers.LevelDetailsSerializer


class WeekListAPIView(ListAPIView):

    serializer_class = serializers.WeekListSerializer
    # pagination_class = CustomPagination

    def get_queryset(self):
        level = self.kwargs['level']
        queryset = Week.objects.filter(level__index=level)
        return queryset


class WeekDetailsAPIView(APIView):

    def get(self, request, level, week, format=None):
        queryset = Week.objects.filter(level__index=level, index=week).first()
        data = serializers.WeekDetailsSerializer(queryset).data
        return Response(data)


class DayListAPIView(ListAPIView):
    queryset = Day.objects.all()
    serializer_class = serializers.DayListSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        level = self.kwargs['level']
        week = self.kwargs['week']
        queryset = Day.objects.filter(
            week__level__index=level, week__index=week)
        return queryset


class DayDetailsAPIView(APIView):

    def get(self, request, level, week, day, format=None):
        queryset = Day.objects.filter(
            week__level__index=level, week__index=week, index=day).first()
        data = serializers.DayDetailsSerializer(queryset).data
        return Response(data)


class GrammarPointListAPIView(ListAPIView):
    queryset = GrammarPoint.objects.all()
    serializer_class = serializers.GrammarPointListSerializer
    pagination_class = CustomPagination


# class GrammarPointDetailsAPIView(RetrieveAPIView):
#     queryset = GrammarPoint.objects.all()
#     serializer_class = serializers.GrammarPointDetailsSerializer

class GrammarPointDetailsAPIView(APIView):

    def get(self, request, level, week, day, grammar, format=None):
        queryset = GrammarPoint.objects.prefetch_related('next').filter(
            day__week__level__index=level, day__week__index=week, day=day, index=grammar).first()
        data = serializers.GrammarPointDetailsSerializer(queryset).data
        return Response(data)
