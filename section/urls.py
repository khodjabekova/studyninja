from django.urls import path
from . import views

urlpatterns = [
    path("grammar",views.LevelListAPIView.as_view()),
    path("kanji",views.KanjiListAPIView.as_view()),
    path("kanji/<level>/icons",views.KanjiIconListAPIView.as_view()),
    path("kanji/<pk>",views.KanjiDetailsAPIView.as_view()),
    # path("<level>/",views.LevelDetailsAPIView.as_view()),
    path("grammar/<level>/",views.WeekListAPIView.as_view()),
    # path("<level>/<week>",views.WeekDetailsAPIView.as_view()),
    path("grammar/<level>/<week>/",views.DayListAPIView.as_view()),
    # path("<level>/<week>/<day>",views.DayDetailsAPIView.as_view()),
    path("grammar/<level>/<week>/<day>/",views.GrammarPointListAPIView.as_view()),
    path("grammar/<level>/<week>/<day>/<grammar>",views.GrammarPointDetailsAPIView.as_view()),
]